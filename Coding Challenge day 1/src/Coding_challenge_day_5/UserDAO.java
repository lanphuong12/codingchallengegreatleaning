package Coding_challenge_day_5;

import java.util.ArrayList;


public interface UserDAO {

	public ArrayList<Task> getAllTask();
	public Task GetTaskbyUser(String user);
	public void DisplayAllTask();
	public void AddTask();
	public void UpdateTask();
	public void DeleteTask();
	public void DisplaybyAssignTask(String assign);
	public void DisplayResulftSearch();
	public void CreateAccount();
	public void SoftTask();
}
