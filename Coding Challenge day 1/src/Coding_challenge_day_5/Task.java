package Coding_challenge_day_5;

import java.sql.Date;
import java.util.Scanner;

public class Task {

	private int TaskId;
	private String TaskTitle;
	private String TaskText;
	private String assignedTo;
	private Date datecompiteDate;
	public Scanner sc = new Scanner(System.in);
	
	public Task() {
	}

	public Task(int taskId, String taskTitle, String taskText, String assignedTo) {
		TaskId = taskId;
		TaskTitle = taskTitle;
		TaskText = taskText;
		this.assignedTo = assignedTo;
	}
	
	

	public Task(int taskId, String taskTitle, String taskText, String assignedTo, Date datecompiteDate) {
		super();
		TaskId = taskId;
		TaskTitle = taskTitle;
		TaskText = taskText;
		this.assignedTo = assignedTo;
		this.datecompiteDate = datecompiteDate;
	}

	public int getTaskId() {
		return TaskId;
	}

	public void setTaskId(int taskId) {
		TaskId = taskId;
	}

	public String getTaskTitle() {
		return TaskTitle;
	}

	public void setTaskTitle(String taskTitle) {
		TaskTitle = taskTitle;
	}

	public String getTaskText() {
		return TaskText;
	}

	public void setTaskText(String taskText) {
		TaskText = taskText;
	}

	public String getAssignedTo() {
		return assignedTo;
	}

	public void setAssignedTo(String assignedTo) {
		this.assignedTo = assignedTo;
	}

	public Date getDatecompiteDate() {
		return datecompiteDate;
	}

	public void setDatecompiteDate(Date datecompiteDate) {
		this.datecompiteDate = datecompiteDate;
	}

	
}
