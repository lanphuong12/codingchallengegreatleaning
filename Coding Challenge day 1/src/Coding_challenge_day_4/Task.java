package Coding_challenge_day_4;

import java.util.Scanner;

public class Task {

	private int TaskId;
	private String TaskTitle;
	private String TaskText;
	private String assignedTo;
	public Scanner sc = new Scanner(System.in);
	
	public Task() {
	}

	public Task(int taskId, String taskTitle, String taskText, String assignedTo) {
		TaskId = taskId;
		TaskTitle = taskTitle;
		TaskText = taskText;
		this.assignedTo = assignedTo;
	}

	public int getTaskId() {
		return TaskId;
	}

	public void setTaskId(int taskId) {
		TaskId = taskId;
	}

	public String getTaskTitle() {
		return TaskTitle;
	}

	public void setTaskTitle(String taskTitle) {
		TaskTitle = taskTitle;
	}

	public String getTaskText() {
		return TaskText;
	}

	public void setTaskText(String taskText) {
		TaskText = taskText;
	}

	public String getAssignedTo() {
		return assignedTo;
	}

	public void setAssignedTo(String assignedTo) {
		this.assignedTo = assignedTo;
	}


}
